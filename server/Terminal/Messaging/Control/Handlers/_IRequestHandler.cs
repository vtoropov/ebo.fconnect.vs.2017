﻿#region

using MassTransit;
using FConnect.Server.Terminal.Messaging.Connection;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Handlers
{
    public interface IRequestHandler<T> : Consumes<T>.Selected where T : class, IConnectionRequest
    {
    }
}