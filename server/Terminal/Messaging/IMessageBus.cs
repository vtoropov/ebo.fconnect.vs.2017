﻿#region

using System.Threading.Tasks;
using MassTransit;                           // https://www.nuget.org/packages/MassTransit/2.10.2;

#endregion

namespace FConnect.Server.Terminal.Messaging
{
    public interface IMessageBus
	{   // the last version of MassTransit has differ interface; and it requires to change the project code;
		MassTransit.IServiceBus Queue { get; }
		Task StartAsync();
    }
}