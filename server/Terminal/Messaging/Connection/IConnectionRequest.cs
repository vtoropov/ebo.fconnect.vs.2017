﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Messaging.Connection
{
    public interface IConnectionRequest
    {
        Guid ConnectionId { get; set; }
    }
}