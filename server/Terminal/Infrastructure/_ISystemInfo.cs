﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Infrastructure
{
    public interface ISystemInfo
    {
        DateTime Now();
        Guid Guid();
    }
}