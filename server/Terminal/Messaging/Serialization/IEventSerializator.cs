﻿#region

using System;
using System.IO;
using FConnect.Server.Terminal.Messaging.Connection;

#endregion

namespace FConnect.Server.Terminal.Messaging.Serialization
{
    public interface IEventSerializator
    {
        void Serialize(Guid connectionId, IConnectionEvent eventObject, Stream output);
        IConnectionRequest Deserialize(Guid connectionId, Stream source, out Type type);
    }
}