﻿#region

using System.Collections.Concurrent;
using System.Threading;

using FConnect.Server.WebSocketAPI.Authentication;

#endregion

namespace FConnect.Server.Api.Services.LocalSystem
{
    public class ScreenShareService
    {
        public ScreenShareService()
        {
            Streams = new ConcurrentDictionary<AuthClient, Thread>();
        }

        public static ConcurrentDictionary<AuthClient, Thread> Streams { get; set; }
    }
}