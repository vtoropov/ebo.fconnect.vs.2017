﻿#region

using FConnect.Server.Terminal.Infrastructure;
using FConnect.Server.Terminal.Messaging.Control.Events;
using FConnect.Server.Terminal.Messaging.Control.Requests;
using FConnect.Server.Terminal.Session;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Handlers
{
    public class CloseTerminalRequestHandler : IRequestHandler<CloseTerminalRequest>
    {
        private readonly ConnectionManager _connectionManager;
        private readonly ILogger _log;

        public CloseTerminalRequestHandler(ConnectionManager sessions, ILogger log)
        {
            _connectionManager = sessions;
            _log = log;
        }

        public bool Accept(CloseTerminalRequest message)
        {
            return true;
        }

        public void Consume(CloseTerminalRequest message)
        {
            var connection = _connectionManager.GetConnection(message.ConnectionId);
            connection.Close(message.TerminalId);
            connection.Push(new ClosedTerminalEvent {TerminalId = message.TerminalId});
        }
    }
}