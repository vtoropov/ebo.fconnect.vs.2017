﻿#region

using System;
using FConnect.Server.Terminal.Messaging.Connection;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Requests
{
    [Serializable]
    public class CreateTerminalRequest : IConnectionRequest
    {
        public string TerminalType { get; set; }
        public int CorrelationId { get; set; }
        public Guid ConnectionId { get; set; }
    }
}