﻿#region

using System;
using FConnect.Server.Terminal.Messaging.Connection;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Requests
{
    public interface ITerminalRequest : IConnectionRequest
    {
        Guid TerminalId { get; set; }
    }
}