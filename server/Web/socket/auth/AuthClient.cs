﻿#region

using System;
using System.Collections.Concurrent;
using System.Security;
using FConnect.Server.Api.Network.Messages;

#endregion

namespace FConnect.Server.WebSocketAPI.Authentication
{
    public class AuthClient
    {
        public AuthClient()
        {
            LastUpdate = DateTime.Now;
            Authenticated = false;
            AesShook = false;
        }

        public ConcurrentDictionary<int, MessageQueueManager> MessageQueueManagers { get; set; }
 
        public DateTime LastUpdate { get; set; }
        public bool Authenticated { get; set; }
        public SecureString PrivateKey { get; set; }
        public SecureString PublicKey { get; set; }
        public SecureString AesKey { get; set; }
        public SecureString AesIv { get; set; }
        public bool AesShook { get; set; }
        public bool ShutDownScreenShare { get; set; }
    }
}