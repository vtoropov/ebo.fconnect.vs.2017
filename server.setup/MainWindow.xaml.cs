﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using FConnect.Setup.Tools;
using Hardcodet.Wpf.TaskbarNotification;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace FConnect.Setup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Utils.KillAllButMe();
           
            if (Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Count() > 1) return;
            InitializeComponent();
            var service = new Task(SetText);
            service.Start();
            this.Hide();
            TrayIcon.TrayBalloonTipClosed += (sender, e) => {
                var thisIcon = (NotifyIcon)sender;
                thisIcon.Visible = false;
                thisIcon.Dispose();
            };

            TrayIcon.ShowBalloonTip("FConnect", "FConnect setup started.", BalloonIcon.Info);

        }
     

        private void SetText()
        {
            while (true)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    FConnectInstalledLabel.Content = Utils.FConnectInstalled() ? "FConnect is Installed" : "FConnect is Not Installed";
                    ServerStatusBlock.Text = $"Server: {GetStatusString(Utils.ServerRunning())}";
                    AgentStatusBlock.Text = $"Agent: {GetStatusString(Utils.AgentRunning())}";
                    StateButton.Content = Utils.ServerRunning() ? "Kill FConnect" : "Start FConnect";
                    TrayIcon.ToolTipText = $"Server: {GetStatusString(Utils.ServerRunning())}\nAgent: {GetStatusString(Utils.AgentRunning())}";
                });

                Thread.Sleep(1000);
            }
          
        }

        private string GetStatusString(bool status)
        {
            return status ? "Running" : "Stopped";
        }

        private bool ShowMissingError()
        {
            if (Utils.FConnectInstalled()) return false;

            TrayIcon.ShowBalloonTip("FConnect", "The server is not currently installed", BalloonIcon.Error);
            return true;
        }
        private void StateButton_Click(object sender, RoutedEventArgs e)
        {
            if (ShowMissingError()) return;
            if (Utils.ServerRunning() || Utils.AgentRunning())
            {
                Utils.KillFConnect();
                TrayIcon.ShowBalloonTip("FConnect", "The server has been stopped", BalloonIcon.Info);
            }
            else
            {
                Utils.RestartService();
                TrayIcon.ShowBalloonTip("FConnect", "The server has been restarted", BalloonIcon.Info);

            }
        }

        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            if (ShowMissingError()) return;
            Utils.RestartService();
            TrayIcon.ShowBalloonTip("FConnect", "The server has been restarted", BalloonIcon.Info);
        }

        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            var ip = Utils.GetDisplayAddress();
            var httpPort = 22006;
            Process.Start($"http://{ip}:{httpPort}");
        
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://fconnect.io/signup/");
        }

        private void Survey_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://fconnect.io/survey/");
        }

        private void Guide_CLick(object sender, RoutedEventArgs e)
        {
            Process.Start("https://fconnect.io/guide/");
        }

        private void OpenGui(object sender, RoutedEventArgs e)
        {
            this.Show();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;

        }

        private void CloseApp(object sender, RoutedEventArgs e)
        {
            TrayIcon.Dispose();
            TrayIcon = null;
            Application.Current.Shutdown();
        }
    }
}
