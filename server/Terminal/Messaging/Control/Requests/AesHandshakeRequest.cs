﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Requests
{
    [Serializable]
    public class AesHandshakeRequest : ITerminalRequest
    {
        public bool AesShook { get; set; }
        public Guid TerminalId { get; set; }
        public Guid ConnectionId { get; set; }
    }
}