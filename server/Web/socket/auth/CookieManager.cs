﻿#region

using System;
using vtortola.WebSockets;

#endregion

namespace FConnect.Server.WebSocketAPI.Authentication
{
    public static class CookieManager
    {
        public static Guid GetConnectionId(WebSocket clientSocket)
        {
			string key_ = "ConnectionId";
            Guid connectionId;
            var cookie = clientSocket.HttpRequest.Cookies[key_] ??
                         clientSocket.HttpResponse.Cookies[key_];
            if (cookie == null || !Guid.TryParse(cookie.Value, out connectionId))
                return Guid.Empty;
			else
				return connectionId;
        }
    }
}