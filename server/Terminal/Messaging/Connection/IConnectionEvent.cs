﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Messaging.Connection
{
    public interface IConnectionEvent
    {
        Guid ConnectionId { get; set; }
    }
}