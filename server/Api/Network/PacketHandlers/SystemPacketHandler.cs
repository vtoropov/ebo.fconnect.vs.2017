﻿#region

using FConnect.Server.Api.Network.Messages;
using FConnect.Server.Api.Network.Models;
using FConnect.Server.WebSocketAPI.Authentication;
using vtortola.WebSockets;

#endregion

namespace FConnect.Server.Api.Network.PacketHandlers
{
    public class SystemPacketHandler : PacketHandler
    {
        private MessageBuilder _builder;
        private AuthClient _authClient;
        private Packet _packet;
        private WebSocket _client;


        /// <summary>
        /// Builds the system information object to json
        /// </summary>
        public void GetSystemInformation()
        {
            _builder.WriteMessage(SystemInformation.ToObject());
        }

        public override void HandlePacket(Packet packet)
        {
            _client = packet.Client;
            _authClient = packet.AuthClient;
            _packet = packet;
            _builder = new MessageBuilder(_authClient, _client, _packet.EndPointName, _packet.SyncKey);
            switch (_packet.EndPoint)
            {
                case PacketManager.EndPoints.RequestSystemInformation:
                    GetSystemInformation();
                    break;
            }
        }
    }
}