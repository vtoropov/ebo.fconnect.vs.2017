﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Requests
{
    [Serializable]
    public class CloseTerminalRequest : ITerminalRequest
    {
        public Guid TerminalId { get; set; }
        public Guid ConnectionId { get; set; }
    }
}