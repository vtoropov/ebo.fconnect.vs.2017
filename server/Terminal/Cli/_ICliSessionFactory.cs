﻿namespace FConnect.Server.Terminal.Cli
{
    public interface ICliSessionFactory
    {
        string Type { get; }
        ICliSession Create();
    }
}