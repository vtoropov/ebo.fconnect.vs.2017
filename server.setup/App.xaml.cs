﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using FConnect.Setup.Tools;

namespace FConnect.Setup
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnExit(ExitEventArgs e)
        {
            
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
           
        }
    }
}
