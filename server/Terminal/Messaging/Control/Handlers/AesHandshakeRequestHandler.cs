﻿#region

using FConnect.Server.Terminal.Infrastructure;
using FConnect.Server.Terminal.Messaging.Control.Requests;
using FConnect.Server.Terminal.Session;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Handlers
{
    public class AesHandshakeRequestHandler : IRequestHandler<AesHandshakeRequest>
    {
        private readonly ConnectionManager _connectionManager;
        private readonly ILogger _log;

        public AesHandshakeRequestHandler(ConnectionManager sessions, ILogger log)
        {
            _connectionManager = sessions;
            _log = log;
        }

        public bool Accept(AesHandshakeRequest message)
        {
            return true;
        }

        public void Consume(AesHandshakeRequest message)
        {
            var connection = _connectionManager.GetConnection(message.ConnectionId);
            connection.AesInfo();
        }
    }
}