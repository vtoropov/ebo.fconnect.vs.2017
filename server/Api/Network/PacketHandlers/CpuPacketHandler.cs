﻿#region

using FConnect.Server.Api.Network.Messages;
using FConnect.Server.Api.Network.Models;
using FConnect.Server.WebSocketAPI.Authentication;
using vtortola.WebSockets;

#endregion

namespace FConnect.Server.Api.Network.PacketHandlers
{
    public class CpuPacketHandler : PacketHandler
    {
        private MessageBuilder _builder;
        private AuthClient _authClient;
        private Packet _packet;
        private WebSocket _client;


        public void GetCpuInformation()
        {
            _builder.WriteMessage(CpuInformation.ToObject());
        }

  
        public override void HandlePacket(Packet packet)
        {
            _client = packet.Client;
            _authClient = packet.AuthClient;
            _packet = packet;
            _builder = new MessageBuilder(_authClient, _client, _packet.EndPointName, _packet.SyncKey);
            switch (_packet.EndPoint)
            {
                case PacketManager.EndPoints.RequestCpuInformation:
                    GetCpuInformation();
                    break;
            }
        }
    }
}