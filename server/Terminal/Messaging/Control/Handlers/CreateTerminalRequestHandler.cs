﻿#region

using System;
using System.Linq;
using FConnect.Server.Terminal.Cli;
using FConnect.Server.Terminal.Infrastructure;
using FConnect.Server.Terminal.Messaging.Control.Events;
using FConnect.Server.Terminal.Messaging.Control.Requests;
using FConnect.Server.Terminal.Session;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Handlers
{
    public class CreateTerminalRequestHandler : IRequestHandler<CreateTerminalRequest>
    {
        private readonly ConnectionManager _connections;
        private readonly ICliSessionFactory[] _factories;
        private readonly ILogger _log;
        private readonly ISystemInfo _sysinfo;

        public CreateTerminalRequestHandler(ConnectionManager sessions, ICliSessionFactory[] factories, ILogger log,
            ISystemInfo sysinfo)
        {
            _factories = factories;
            _connections = sessions;
            _log = log;
            _sysinfo = sysinfo;
        }

        public bool Accept(CreateTerminalRequest message)
        {
            return true;
        }

        public void Consume(CreateTerminalRequest message)
        {
            try
            {
                var factory = _factories.SingleOrDefault(f => f.Type == message.TerminalType);
                if (factory == null)
                    throw new ArgumentException("There is no factory for this type");
                var connection = _connections.GetConnection(message.ConnectionId);
                if (connection == null)
                    throw new ArgumentException("The connection does not exists");
                if (!connection.AesShook)
                {
                    Console.WriteLine("AES not shook, cannot create terminal");
                    return;
                }
                var id = _sysinfo.Guid();
                var cli = factory.Create();
                connection.Append(id, cli);
                connection.Push(new CreatedTerminalEvent
                {
                    TerminalId = id,
                    TerminalType = message.TerminalType,
                    CurrentPath = cli.CurrentPath,
                    CorrelationId = message.CorrelationId
                });
                if (!connection.IsAuthed)
                {
                    cli.Output("Please Login to continue, type: ulterius-auth", 0, true, false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}