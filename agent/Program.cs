﻿#region

using Topshelf;
using Warden.Core;

#endregion

namespace FConnect.Agent
{

    internal class Program
    {
        private static void Main(string[] args)
        {
            WardenManager.Initialize(new WardenOptions
            {
                DeepKill = true,
                CleanOnExit = true,
                ReadFileHeaders = false
            });
            HostFactory.Run(x => //1
            {
                x.Service<FConnectAgent>(s => //2
                {
                    s.ConstructUsing(name => new FConnectAgent()); //3
                    s.WhenStarted(tc => tc.Start()); //4
                    s.WhenStopped(tc => tc.Stop());
                    s.WhenSessionChanged((se, e, id) => { se.HandleEvent(e, id); }); //5
                });
                x.OnException(ex =>
                {
                    //TODO Logging
                });
                x.RunAsLocalSystem(); //6
                x.EnableSessionChanged();
                x.EnableServiceRecovery(r => { r.RestartService(1); });
                x.SetDescription("The server that powers Ulterius"); //7
                x.SetDisplayName("FConnect Server"); //8
                x.SetServiceName("FConnect_Server"); //9
                x.StartAutomaticallyDelayed();
            });
        }
    }
}