﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;                            // https://www.nuget.org/packages/topshelf
using Warden.Core;                         // https://www.nuget.org/packages/Warden.NET/1.5.5.1
using Warden.Core.Utils;                   // https://www.nuget.org/packages/Microsoft.Win32.Registry/4.6.0-rc1.19456.4 is required for warden;

namespace FConnect.Agent
{
    public class FConnectAgent
    {
        private readonly string _f_connectPath;
        private WardenProcess   _f_connectInstance;


        public FConnectAgent()
        {
            const string f_srvFileName = "FConnect Server.exe";
			_f_connectPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, f_srvFileName);
        }

        public void Start()
        {
            if (!File.Exists(_f_connectPath))
            {
                throw new InvalidOperationException($"Unable to locate FConnect server at {_f_connectPath}");
            }
            if (Respawn())
            {
                Console.WriteLine("Rainway started as a service;");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Failed to start Rainway;");
                Environment.Exit(1);
            }
        }

        private bool Respawn()
        {
            _f_connectInstance = null;
			_f_connectInstance = WardenProcess.Start(
			_f_connectPath, string.Empty, null, null, true
				).GetAwaiter().GetResult();
            if (_f_connectInstance == null || !_f_connectInstance.IsTreeActive())
            {
                return false;
            }
			_f_connectInstance.OnStateChange += OnOnStateChange;
            return true;
        }

        public void Stop()
        {
            const string _f_connectFileName = "FConnect Server.exe";
            if (_f_connectInstance != null)
            {
				_f_connectInstance?.Kill();
                WardenManager.Flush(_f_connectInstance.Id);
            }
			_f_connectInstance = null;
            EndProcessTree (_f_connectFileName);
        }

        private void EndProcessTree(string imageName)
        {
            try
            {
                var taskKill = new TaskKill
                {
                    Arguments = new List<TaskSwitch>()
                    {
                        TaskSwitch.Force,
                        TaskSwitch.TerminateChildren,
                        TaskSwitch.ImageName.SetValue(imageName)
                    }
                };
                taskKill.Execute(out var output, out var errror);
            }
            catch
            {
                //
            }
        }

        private void OnOnStateChange(object sender, StateEventArgs stateEventArgs)
        {
            if (stateEventArgs.Id == _f_connectInstance.Id && stateEventArgs.State == ProcessState.Dead)
            {
				//Kill the entire tree.
				_f_connectInstance.Kill();
                WardenManager.Flush(_f_connectInstance.Id);
                if (Respawn())
                {
                   Console.WriteLine("Rainway restarted;");
                }
            }
        }

        public void HandleEvent(HostControl hostControl, SessionChangedArguments arg3)
        {
            
        }
    }
}
