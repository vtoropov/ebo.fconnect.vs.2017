﻿using System;

namespace FConnect.Server.Api.Win32.ScreenShare.DesktopDuplication
{
    public class DesktopDuplicationException : Exception
    {
        public DesktopDuplicationException(string message)
            : base(message) { }
    }
}
