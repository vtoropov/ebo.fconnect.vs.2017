﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Cli
{
    public interface ICliSession : IDisposable
    {
        string Type { get; }
        string CurrentPath { get; }
        Action<string, int, bool, bool> Output { get; set; }
        void Input(string value, int commandCorrelationId);
    }
}