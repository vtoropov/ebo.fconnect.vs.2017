﻿#region

using System;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Events
{
    [Serializable]
    public class ClosedTerminalEvent : ITerminalEvent
    {
        public Guid TerminalId { get; set; }
        public Guid ConnectionId { get; set; }
    }
}