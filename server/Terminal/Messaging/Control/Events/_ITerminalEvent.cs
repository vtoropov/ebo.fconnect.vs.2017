﻿#region

using System;
using FConnect.Server.Terminal.Messaging.Connection;

#endregion

namespace FConnect.Server.Terminal.Messaging.Control.Events
{
    public interface ITerminalEvent : IConnectionEvent
    {
        Guid TerminalId { get; set; }
    }
}