﻿#region

using FConnect.Server.Api.Network.Messages;

#endregion

namespace FConnect.Server.Api.Network
{
    public abstract class PacketHandler
    {
        /// <summary>
        /// Usiing the HandlePacket void we are able to handle each endpoint in their own functions 
        /// </summary>
        public abstract void HandlePacket(Packet packet);
    }
}